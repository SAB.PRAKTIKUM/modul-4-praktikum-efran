package org.unpas.sab.modul_4_efran;

import android.database.sqlite.SQLiteDatabase;
import android.database.Cursor;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.widget.ArrayAdapter;
import android.widget.ListView;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.List;

public class ViewMahasiswaActivity extends AppCompatActivity {
    ListView myListView;
    protected DatabaseHelper dbHelper;
    protected SQLiteDatabase db;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_view_mahasiswa);
        myListView = (ListView) findViewById(R.id.IsMhs);
        dbHelper = new DatabaseHelper(this);
        db = dbHelper.getWritableDatabase();
        try{
            LoadDataMahasiswa();
        }catch (Exception e){
            Log.e("masuk","->"+e.getMessage());
        }
    }
    private void LoadDataMahasiswa(){
        ArrayAdapter<String> adapter = new ArrayAdapter<String>(this,
                android.R.layout.simple_list_item_1, getAllData());
        myListView.setAdapter(adapter);
    }
    public List<String> getAllData(){
        List<String>  names = new ArrayList<String>();
        Cursor cursor = dbHelper.getAll(db);
        cursor.moveToFirst();
        while(!cursor.isAfterLast()){
            String data = cursor.getString(0)+" - "
                    + cursor.getString(1)+ " - "
                    + cursor.getString(2);
            names.add(data);
            cursor.moveToNext();
        }
        cursor.close();
        Collections.sort(names);
        return names;
    }

}
