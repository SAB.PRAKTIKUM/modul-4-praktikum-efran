package org.unpas.sab.modul_4_efran;

import android.content.Context;
import android.content.SharedPreferences;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;

public class Session extends AppCompatActivity {
    public static SharedPreferences preferences;
    public static SharedPreferences.Editor editor;
    public static String PREF_NAME = "DataMahasiswa";

    public static void createSignInSession(Context context, String username){
        preferences = context.getSharedPreferences(PREF_NAME, 0);
        editor = preferences.edit();
        editor.putString("username",username);
        editor.commit();
    }
    public static void Logout(Context context){
        preferences = context.getSharedPreferences(PREF_NAME, 0);
        editor = preferences.edit();
        editor.clear();
        editor.commit();
    }
}
