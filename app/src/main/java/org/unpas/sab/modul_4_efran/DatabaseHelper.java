package org.unpas.sab.modul_4_efran;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;

/**
 * Created by admin601 on 3/16/2018.
 */

public class DatabaseHelper extends SQLiteOpenHelper{
    public static final String DATABASE_NAME = "DataMahasiswa";
    private final static String TABLES[] = {"nrp","nama","prodi"};
    private final static String NAMA_TABLE = "table_mahasiswa";

    public DatabaseHelper(Context context) {
        super(context, DATABASE_NAME,null, 1);
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
    }
    public void insertMahasiswa(SQLiteDatabase db, String nrp,String nama,String prodi){
        ContentValues contentValues= new ContentValues();
        contentValues.put("nrp",nrp);
        contentValues.put("nama",nama);
        contentValues.put("prodi",prodi);
        db.insert(NAMA_TABLE, null, contentValues);
        Log.e("MSG : ","insert succes");
    }
    public void deleteMahasiswa(SQLiteDatabase db, String nrp){
        db.delete(NAMA_TABLE, "nrp = "+nrp, null);
    }
    public Cursor getAll(SQLiteDatabase db){
        return db.query(NAMA_TABLE,TABLES,null,null,null,null,null);
    }
    public void createMahasiswaTable(SQLiteDatabase db){
        db.execSQL("CREATE TABLE if not exists "+NAMA_TABLE + "(nrp TEXT PRIMARY KEY, nama TEXT, prodi TEXT);");
        Log.e("MSG : ","create database succes" );
    }

}
