package org.unpas.sab.modul_4_efran;

import android.database.sqlite.SQLiteDatabase;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

public class AddMahasiswaActivity extends AppCompatActivity {

    EditText nama,nrp,prodi;
    Button insert;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add_mahasiswa);

        nama = (EditText) findViewById(R.id.namaMhs);
        nrp = (EditText) findViewById(R.id.nrpMhs);
        prodi = (EditText) findViewById(R.id.prodi);
        insert = (Button) findViewById(R.id.btnInsert);

        insert.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                String sNRP = nrp.getText().toString().trim();
                String sNama = nama.getText().toString().trim();
                String sProdi = prodi.getText().toString().trim();
                DatabaseHelper dbHelper = new DatabaseHelper(AddMahasiswaActivity.this);
                SQLiteDatabase db = dbHelper.getWritableDatabase();
                dbHelper.createMahasiswaTable(db);
                dbHelper.insertMahasiswa(db,sNRP,sNama,sProdi);
                Toast.makeText(AddMahasiswaActivity.this, "Insert Data Berhasil", Toast.LENGTH_LONG).show();

                nrp.setText("");
                nama.setText("");
                prodi.setText("");
            }
        });
    }
}
